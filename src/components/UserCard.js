// import { useState,useContext,useEffect } from "react";
// import { Form, Button, Card } from "react-bootstrap";
// import {Navigate, useNavigate} from 'react-router-dom';
// import Swal from 'sweetalert2';


// import UserContext from "../UserContext"
// export default function AdminDashboard() 
// {
  
//   const [name, setName] = useState("");
//   const [description, setDescription] = useState("");
//   const [price, setPrice] = useState("");
//   const [quantity, setQuantity] = useState("");

//   const [product, setProduct] = useState({});

//   const {user} = useContext(UserContext);
//   const navigate = useNavigate();

//     function CreateNewProduct(e)
//     {
//           e.preventDefault();

//       fetch(`${process.env.REACT_APP_API_URL}/products/create`,
//       {
//         method: "POST",
//         headers:
//               {
//                 "Content-Type" : "application/json",
//                 Authorization: `Bearer ${localStorage.getItem("token")}`
//               },
//         body: JSON.stringify(
//               {
            
//                 name: name,
//                 description: description,
//                 price: price,
//                quantity: quantity
               
               
//               }) 
//       }).then(res =>res.json()).then(data =>
//       {
//         console.log(data)
//         if (data.error)
//         {
//           Swal.fire({
//             title: `Got an error`,
//                     icon: "error",
                  
//           })
//         }
//         else
//         {
//           Swal.fire({
//                     title: `New product Created`,
//                     icon: "success",
                  
//                   })

//           console.log(data.product)
//           navigate(`/allActive`);
//         }
//       })
//     };

 
//  // This following section will display the form that takes the input from the user.
//  return (
//   <div className="mt-3 mb-3 justify-content-md-center">
//       <Card style={{ width: "50rem" }}>
//         <Card.Body>
//           <Card.Title>Create Product</Card.Title>
//           <Form onSubmit={(e) => CreateNewProduct(e)}>

           

//             <Form.Group controlId="formProductName">
//               <Form.Label>Product Name</Form.Label>
//               <Form.Control
//                 type="text"
//                 placeholder="Enter product name"
//                 value={name}
//                 onChange={(e) => setName(e.target.value)}
//                 required
//               />
//             </Form.Group>

//             <Form.Group controlId="formProductDescription">
//               <Form.Label>Product Description</Form.Label>
//               <Form.Control
//                 as="textarea"
//                 rows={3}
//                 placeholder="Enter product description"
//                 value={description}
//                 onChange={(e) => setDescription(e.target.value)}
//                 required
//               />
//             </Form.Group>

//             <Form.Group controlId="formProductPrice">
//               <Form.Label>Price</Form.Label>
//               <Form.Control
//                 type="number"
//                 placeholder="Enter price"
//                 value={price}
//                 onChange={(e) => setPrice(e.target.value)}
//                 required
//               />
//             </Form.Group>

          

//             <Form.Group controlId="formProductQuantity">
//               <Form.Label>Quantity</Form.Label>
//               <Form.Control
//                 type="number"
//                 placeholder="Enter Quantity"
//                 value={quantity}
//                 onChange={(e) => setQuantity(e.target.value)}
//                 required
//               />
//             </Form.Group>

//             <Button variant="primary" type="submit">
//               Create Product
//             </Button>

//           </Form>
//         </Card.Body>
//       </Card>
//     </div>
//   );
// }





// // import { useState, useEffect } from 'react';
// // import { Link } from 'react-router-dom';
// // import { Button, Row, Col, Card } from 'react-bootstrap';

// // export default function AdminDashboard({ details }) {
// //   const { _id, name, description, price, isActive, createdOn, orders } = details;

// //   return (
// //     <>
// //     <h1>Product List</h1>

// // {/*      <div className="d-flex justify-content-between align-items-center mb-3">
// //         <h1>Admin Dashboard</h1>
// //         <Button className="bg-primary" as={Link} to={`/create-product`}>
// //           Create Product
// //         </Button>
// //       </div>*/}


// //       <Row className="mb-3">
// //         <Col xs={12}>
// //           <Card className="cardHighlight p-0">
// //           <Card.Header>
// //             <h4>hello</h4>
// //           </Card.Header>
// //             <Card.Body>
// //               <Card.Title>
// //                 <h4>{name}</h4>
// //               </Card.Title>
// //               <Card.Subtitle>Description111</Card.Subtitle>
// //               <Card.Text>{description}</Card.Text>
// //               <Card.Subtitle>Price</Card.Subtitle>
// //               <Card.Text>{price}</Card.Text>
// //               <Card.Subtitle>Available</Card.Subtitle>
// //               <Card.Text>{isActive ? 'Yes' : 'No'}</Card.Text>
// //               <Card.Subtitle>Created On</Card.Subtitle>
// //               <Card.Text>{new Date(createdOn).toDateString()}</Card.Text>

// //               <Row className="mt-3">
// //                 <Col>
// //                   <Button
// //                     className="bg-secondary"
// //                     as={Link}
// //                     to={`/update-product/${_id}`}
// //                   >
// //                     Update Product
// //                   </Button>
// //                   <Button
// //                     className="bg-secondary"
// //                     as={Link}
// //                     to={`/archive-product/${_id}`}
// //                   >
// //                     Archived Product
// //                   </Button>
// //                 </Col>
// //               </Row>
// //             </Card.Body>
// //           </Card>
// //         </Col>
// //       </Row>
// //     </>
// //   );
// // }
