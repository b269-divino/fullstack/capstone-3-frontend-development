import {useState, useEffect} from 'react';

// import coursesData from '../data/coursesData';
import ProductCard from '../components/ProductCard';

export default function Checkout() {

    // console.log(coursesData);

    // const courses = coursesData.map(course => {
    //  return (
    //      < CourseCard key={course.id} course = {course} />
    //  )
    // })


    const [products, setProducts] = useState([]);

useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/allActive`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setProducts(data.map(product => {
                return(
                    <ProductCard key={product._id} product={product} />
                )
            }))
        })
    }, [])



    return (
        <>
        {products}
        </>
    )
}

/*
PROPS
Way to declare props drilling
We can pass information from one component to another using props (props drilling
Curly braces {} are used for props to signify that we are providing/passing information from one component to another using JS expression
*/
