//update product

import { useState, useEffect, useContext } from "react";

import UserContext from "../UserContext";

import { Form, Button } from "react-bootstrap";

import Swal from "sweetalert2";

import { useParams, useNavigate } from "react-router-dom";

export default function UpdateProduct({ product }) {
  const { user } = useContext(UserContext);

  const navigate = useNavigate();
  // "useParams" hook that will allow us to retrieve the productId passed via URL params
  // http://localhost:3000/products/642b9ee764ba1150b3a1a4e1
  const { productId } = useParams();

  // to store values of the input fields
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  // to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  const updateProduct = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "Successfully updated",
            icon: "success",
            text: "You have successfully updated this product.",
          });

          navigate("/products");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };

  useEffect(() => {
    if (name !== "" || description !== "" || price !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, description, price]);

  return (
    <Form onSubmit={(e) => updateProduct(e)}>
      <Form.Group controlId="name">
        <Form.Label>Product Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Product Name Here"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="description">
        <Form.Label>Description</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Description Here"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="price">
        <Form.Label>Price</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter Price Here"
          value={price}
          onChange={(e) => setPrice(e.target.value)}
        />
      </Form.Group>

      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
