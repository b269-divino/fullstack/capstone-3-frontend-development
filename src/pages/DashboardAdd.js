// import { useState,useContext,useEffect } from "react";
// import { Form, Button, Card } from "react-bootstrap";
// import {Navigate, useNavigate} from 'react-router-dom';
// import Swal from 'sweetalert2';


// import UserContext from "../UserContext"
// export default function DashboardAdd() 
// {
  
//   const [name, setName] = useState("");
//   const [description, setDescription] = useState("");
//   const [price, setPrice] = useState("");
//   const [quantity, setQuantity] = useState("");

//   const [product, setProduct] = useState({});

//   const {user} = useContext(UserContext);
//   const navigate = useNavigate();

//     function CreateNewProduct(e)
//     {
//           e.preventDefault();

//       fetch(`${process.env.REACT_APP_API_URL}/products/create`,
//       {
//         method: "POST",
//         headers:
//               {
//                 "Content-Type" : "application/json",
//                 Authorization: `Bearer ${localStorage.getItem("token")}`
//               },
//         body: JSON.stringify(
//               {
            
//                 name: name,
//                 description: description,
//                 price: price,
//                quantity: quantity
               
               
//               }) 
//       }).then(res =>res.json()).then(data =>
//       {
//         console.log(data)
//         if (data.error)
//         {
//           Swal.fire({
//             title: `Got an error`,
//                     icon: "error",
                  
//           })
//         }
//         else
//         {
//           Swal.fire({
//                     title: `New product Created`,
//                     icon: "success",
                  
//                   })

//           console.log(data.product)
//           navigate(`/productCatalog`);
//         }
//       })
//     };

 
//  // This following section will display the form that takes the input from the user.
//  return (
//   <div className="mt-3 mb-3 justify-content-md-center">
//       <Card style={{ width: "50rem" }}>
//         <Card.Body>
//           <Card.Title>Create Product</Card.Title>
//           <Form onSubmit={(e) => CreateNewProduct(e)}>

           

//             <Form.Group controlId="formProductName">
//               <Form.Label>Product Name</Form.Label>
//               <Form.Control
//                 type="text"
//                 placeholder="Enter product name"
//                 value={name}
//                 onChange={(e) => setName(e.target.value)}
//                 required
//               />
//             </Form.Group>

//             <Form.Group controlId="formProductDescription">
//               <Form.Label>Product Description</Form.Label>
//               <Form.Control
//                 as="textarea"
//                 rows={3}
//                 placeholder="Enter product description"
//                 value={description}
//                 onChange={(e) => setDescription(e.target.value)}
//                 required
//               />
//             </Form.Group>

//             <Form.Group controlId="formProductPrice">
//               <Form.Label>Price</Form.Label>
//               <Form.Control
//                 type="number"
//                 placeholder="Enter price"
//                 value={price}
//                 onChange={(e) => setPrice(e.target.value)}
//                 required
//               />
//             </Form.Group>

          

//             <Form.Group controlId="formProductQuantity">
//               <Form.Label>Quantity</Form.Label>
//               <Form.Control
//                 type="number"
//                 placeholder="Enter Quantity"
//                 value={quantity}
//                 onChange={(e) => setQuantity(e.target.value)}
//                 required
//               />
//             </Form.Group>

//             <Button variant="primary" type="submit">
//               Create Product
//             </Button>

//           </Form>
//         </Card.Body>
//       </Card>
//     </div>
//   );
// }
