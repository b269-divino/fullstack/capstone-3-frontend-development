import {useState, useEffect} from 'react';
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import AdminDashboard from './components/UserCard';
import CreateProducts from './components/CreateProduct';



import Home from './pages/Home';
import Products from './pages/Products';
// import Courses from './pages/Courses';
import Dashboard from './pages/Dashboard';
// 
import Update from './pages/Update';
import Checkout from './pages/Checkout';

import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

import {Container} from 'react-bootstrap';

import {UserProvider} from './UserContext';
// import DashboardAdd from './pages/DashboardAdd';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import './App.css';

function App() {

  // const [user, setUser] = useState({email: localStorage.getItem('email')});

  const [user, setUser] = useState({

    id: null,
    isAdmin: null

});

  const unsetUser = () => {
    localStorage.clear();
  }

useEffect(() => {
  fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(res => res.json())
  .then(data => {

    // User is logged in
    if(typeof data._id !== "undefined") {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    }
    // user is logged out
    else {
      setUser({
        id: null,
        isAdmin: null
      })
    }

  })
}, [])


  return (
    // <></> fragments - common pattern in React for component to return multiple elements
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        < AppNavbar/>
        <Container>
        <Routes>
          < Route path="/" element={<Home/>}/>
          < Route path="/products" element={<Products/>}/>
          < Route path="/products/:productId" element={<ProductView/>}/>
          < Route path="/register" element={<Register/>}/>
          < Route path="/dashboard" element={<Dashboard/>}/>
          {/*< Route path="/DashboardAdd" element={<DashboardAdd/>}/>*/}
          {/*< Route path="/products/:productId" element={<AdminDashboard/>}/>*/}
          < Route path="/create" element={<CreateProducts/>}/>
          < Route path="/update" element={<Update/>}/>
          <Route path="/update/:productId" element={<Update />} />
          < Route path="/cart" element={<Checkout/>}/>
          < Route path="/cart/:userId" element={<ProductView/>}/>
          < Route path="/login" element={<Login />}/>
          < Route path="/logout" element={<Logout/>}/>
          < Route path="/*" element={<Error />}/>
        </Routes>
        </Container>
      </Router>
      </UserProvider>
    </>
  );
}

export default App;




